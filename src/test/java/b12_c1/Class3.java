package b12_c1;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Class3 {
	
	public static AppiumDriver<MobileElement> driver;  //AndroidDriver
	
	public static DesiredCapabilities cap;
	
	
	@BeforeTest
	
	public void setup() throws MalformedURLException{
		
		String filepath = "D://Appium//Training//B-12//EriBank.apk";
		
		
		cap = new DesiredCapabilities();
		
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "192.168.56.101:5555");
		cap.setCapability(MobileCapabilityType.APP, filepath);
		
		
		driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),cap);
		
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		
		
		
	}
	
	@Test
	
	public void firstTest() throws InterruptedException{
		
		Thread.sleep(5000L);
		System.out.println("first test executing");
		driver.findElementById("com.experitest.ExperiBank:id/usernameTextField").sendKeys("company");
		
		driver.findElementById("com.experitest.ExperiBank:id/passwordTextField").sendKeys("company");
		
		driver.tap(1,driver.findElementById("com.experitest.ExperiBank:id/loginButton"), 1);
		System.out.println("first test executed");
		Thread.sleep(5000L);
		driver.tap(1,driver.findElementById("com.experitest.ExperiBank:id/logoutButton"), 1);
		Thread.sleep(10000L);
	}
	
	@AfterTest
	
	public void quitdriver(){
		driver.quit();
	}
	
	

}
